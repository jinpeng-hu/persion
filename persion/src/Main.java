
import java.io.*;

public class Main {

    public static void read() {
        String string = null;

        int a[] = new int[52];

        char b[] = new char[52];
        b[0] = 'a';
        b[1] = 'b';
        b[2] = 'c';
        b[3] = 'd';
        b[4] = 'e';
        b[5] = 'f';
        b[6] = 'g';
        b[7] = 'h';
        b[8] = 'i';
        b[9] = 'j';
        b[10] = 'k';
        b[11] = 'l';
        b[12] = 'm';
        b[13] = 'n';
        b[14] = 'o';
        b[15] = 'p';
        b[16] = 'q';
        b[17] = 'r';
        b[18] = 's';
        b[19] = 't';
        b[20] = 'u';
        b[21] = 'v';
        b[22] = 'w';
        b[23] = 'x';
        b[24] = 'y';
        b[25] = 'z';
        b[26] = 'A';
        b[27] = 'B';
        b[28] = 'C';
        b[29] = 'D';
        b[30] = 'E';
        b[31] = 'F';
        b[32] = 'G';
        b[33] = 'H';
        b[34] = 'I';
        b[35] = 'J';
        b[36] = 'K';
        b[37] = 'L';
        b[38] = 'M';
        b[39] = 'N';
        b[40] = 'O';
        b[41] = 'P';
        b[42] = 'Q';
        b[43] = 'R';
        b[44] = 'S';
        b[45] = 'T';
        b[46] = 'U';
        b[47] = 'V';
        b[48] = 'W';
        b[49] = 'X';
        b[50] = 'Y';
        b[51] = 'Z';

        try {
            // 在给定从中读取数据的文件名的情况下创建一个新 FileReader
            FileReader fr = new FileReader("C:\\Users\\Excursion\\Desktop\\Gone_with_the_wind.txt");

            // 创建一个使用默认大小输入缓冲区的缓冲字符输入流
            BufferedReader br = new BufferedReader(fr);

            while (null != (string = br.readLine())) {
                int sum = string.length();
                for (int i = 0; i < sum; i++) {
                    for (int j = 0; j < 52; j++) {
                        if (string.charAt(i) == b[j]) {
                            a[j]++;
                        }
                    }
                }
            }
            fr.close();
            br.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }

        int max = 0;
        double s = 0;
        for (int i = 0; i < 52; i++) {
            s += a[i];
        }
        System.out.println("总次数" + s);
        for (int j = 0; j < 52; j++) {
            int c = 0;
            for (int i = 0; i < 52; i++) {

                if (a[i] > max) {
                    max = a[i];
                    c = i;
                }
                if (i == 51) {
                    System.out.print(b[c] + "  次数   " + a[c] + "   出现率   ");
                    System.out.println(String.format("%.2f", a[c] / s * 100)
                            + "%");
                    a[c] = 0;
                    c = 0;
                    max = 0;
                }
            }
        }
    }

    public static void main(String[] args) {
        read();
    }
}